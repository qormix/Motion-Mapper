const { app, BrowserWindow, Menu } = require('electron')

const path = require('path')
const url = require('url')

let window

function create() {

    window = new BrowserWindow({ width: 800, height: 600 })

    window.loadURL(url.format({
        pathname: path.join(__dirname, 'app/index.html'),
        protocol: 'file',
        slashes: true
    }))

    window.toggleDevTools()
    window.on('closed', function () { window = null })
}

app.on('ready', create)

app.on('window-all-closed', function () { if (process.platform !== 'darwin') app.quit() })

app.on('activate', function () { if (window === null) create() })

Menu