import React from 'react'
import ReactDOM from 'react-dom'
import { AppBar, Drawer } from 'material-ui'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

const App = () => (
    <MuiThemeProvider>
      <AppBar/>
    </MuiThemeProvider>
);

ReactDOM.render(<App/>, document.getElementById('root'))